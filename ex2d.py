s = input("letra = ").lower()
s = s[0]
# solucao 1
if s == 'a' or  s == 'e' or s == 'i' or s == 'o' or s == 'u':
    print('vogal')
else:
    print('consoante')

# solucao 2
if s in ['a', 'e', 'i', 'o', 'u']:
    print('vogal')
else:
    print('consoante')

# solucao 3
if s in 'aeiou':
    print('vogal')
else:
    print('consoante')
